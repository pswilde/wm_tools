type
  Info* = object
    title*: string
    full_text*: string
    html_text*: string
    short_text*: string
    args*: seq[string]

proc newInfo*(str: string = "Info"): Info =
  var title = str
  return Info(title: title)
