
type
  PWGen* = object
    to_terminal*: bool
    word_len*: int
    digits*: int
    number*: int

proc newPWGen*(): PWGen =
  var pw = PWGen()
  pw.word_len = 5
  pw.digits = 4
  pw.number = 10
  return pw
