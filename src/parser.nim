import os

import argparse

import common
import common/display
import model/pwgen
import model/volume
import model/brightness
import model/screenshot
import model/tides
import model/wallpapurr

proc parseArgs*() =
  let params =  commandLineParams()
  var p = newParser:
    help("WMTools : a set of tools to output option to your program of choice i.e. Rofi")
    arg("input",help="the tool to run, i.e. furrytime, pingclock, volurrme, etc.")
    arg("others", nargs = -1)
    flag("-o","--output",help="outputs to stdout instead of something else")
  try:
    var opts = p.parse(params)
    # TODO sort this but out, handle args for all modules, etc.
    myConfig.to_stdout = opts.output
    case opts.input
    of "furrytime", "fuzzytime", "time":
      myConfig.run = FurryTime
    of "pingclock", "pingclurrk", "ping":
      myConfig.run = PingClock
    of "batturry", "battery", "bat":
      myConfig.run = Batturry
    of "volurrme", "volume", "vol":
      myConfig.run = Volurrme
    of "netwurrk", "network", "net":
      myConfig.run = Netwurrk
    of "wirelurrs", "wireless", "wifi":
      myConfig.run = Wirelurrs
    of "emurrji", "emoji":
      myConfig.run = Emurrji
    of "calendurr", "calender", "cal":
      myConfig.run = Calendurr
    of "remminurr", "remmina", "rdp", "rem":
      myConfig.run = Remminurr
    of "passwurrd", "password", "pw":
      myConfig.run = Passwurrd
    of "passwurrdgeneraturr", "passwordgenerator", "pwgen":
      myConfig.run = PasswurrdGeneraturr
    of "temperaturr", "temperature", "temp":
      myConfig.run = Temperaturr
    of "screenshurrt", "screenshot", "screeny":
      myConfig.run = Screenshurrt
    of "calculaturr", "calculator", "calc":
      myConfig.run = Calculaturr
    of "brightnurrs", "brightness", "bright":
      myConfig.run = Brightnurrs
    of "tideurrl", "tides":
      myConfig.run = Tideurrl
    of "wallpapurr", "wallpaper":
      myConfig.run = Wallpapurr
    else:
      echo p.help
      quit(1)
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)

proc parseVolArgs*(): VolArg =
  var arg: VolArg
  let params = commandLineParams()
  var p = newParser:
    help("Args for volurrme")
    arg("volurrme",help="can only ever be 'volurrme' as you won't have gotten this far otherwise")
    arg("adjust",help="up, down, or mute",default=some(""))
  try:
    var opts = p.parse(params)
    case opts.adjust
    of "volup", "up":
      arg = VolUp
    of "voldown", "down":
      arg = VolDown
    of "mute","volmute":
      arg = VolMute
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
  return arg

proc parseBrightnessArgs*(): BrightnessArg =
  var arg: BrightnessArg
  let params = commandLineParams()
  var p = newParser:
    help("Args for volurrme")
    arg("brightnurrs",help="can only ever be 'brightnurrs' as you won't have gotten this far otherwise")
    arg("adjust",help="up, down, or mute",default=some(""))
  try:
    var opts = p.parse(params)
    case opts.adjust
    of "up":
      arg = BrightUp
    of "down":
      arg = BrightDown
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
  return arg

proc parsePWGenArgs*(): PWGen =
  var gen = newPWGen()
  let params =  commandLineParams()
  var p = newParser:
    help("Args for pw_generaturr")
    arg("pwgen",help="can only ever be 'pwgen' as you won't have gotten this far otherwise")
    flag("-o","--output",help="outputs to terminal instead of something else")
    option("-l","--length",help="Length of the word part of the password")
    option("-d","--digits",help="Length of the number part of the password",default=some(""))
    option("-n","--number",help="Number of passwords to return")
  try:
    var opts = p.parse(params)
    if opts.length != "":
      gen.word_len = parseInt(opts.length)
    if opts.digits != "":
      gen.digits = parseInt(opts.digits)
    if opts.number != "":
      gen.number = parseInt(opts.number)
    gen.to_terminal = not opts.output
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
  return gen

proc parseScreenshotArgs*(): Screenshot =
  var ss = newScreenshot()
  ss.tool = myConfig.screenshot_tool
  let params =  commandLineParams()
  var p = newParser:
    help("Args for screenshurrt")
    arg("screenshurrt",help="can only ever be 'screenshurrt' as you won't have gotten this far otherwise")
    option("-s","--size",help="size/region i.e. region, fullscreen or window")
    option("-t","--tool",help="the tool to take the screenshot, i.e. maim or grim")
  try:
    var opts = p.parse(params)
    if opts.size != "":
      ss.size = opts.size.toScreenshotSize()
    if opts.tool != "":
      ss.tool = opts.tool.toScreenshotTool()
    elif isWayland():
      ss.tool = GRIM
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
  return ss

proc parseTideurrlArgs*(): TideList =
  var t = newTideList()
  let params =  commandLineParams()
  var p = newParser:
    help("Args for tideurrl")
    arg("tideurrl",help="can only ever be 'tideurrl' as you won't have gotten this far otherwise")
    option("-l","--loc",help="location name")
  try:
    var opts = p.parse(params)
    if opts.loc != "":
      t.location = opts.loc
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
  return t

proc parseWallpapurrArgs*(): WPArgs =
  var args = WPArgs()
  let params =  commandLineParams()
  var p = newParser:
    help("Args for wallpapurr")
    arg("wallpapurr",help="can only ever be 'wallpapurr' as you won't have gotten this far otherwise")
    option("-q","--query",help="query name")
    flag("-l","--last",help="last image")
    flag("-n","--unsplash",help="get from unsplash")
  try:
    var opts = p.parse(params)
    if opts.query != "":
      args.query = opts.query
    if opts.last:
      args.last = true
    if opts.unsplash:
      args.from_unsplash = true
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
  return args
