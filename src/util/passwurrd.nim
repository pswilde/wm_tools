import os
import osproc
import re
import strutils

import ../common
import ../output

const pw_store = getHomeDir() & ".password-store/"
var passwords: seq[string] = @[]
let gpg_re = re("(" & pw_store & "|\\.gpg)")

proc parseFiles(path: string) =
  for file in walkDir(path):
    if file.path.endsWith(".gpg"):
      let pw = replace(file.path, gpg_re,"")
      passwords.add(pw)
    elif file.kind == pcDir:
      parseFiles(file.path)

proc getPasswords(): seq[string] =
  parseFiles(pw_store)
  return passwords

proc passwordToClipboard(password: string) =
  discard execCmd("pass show -c " & password)

proc go*() =
  var info = newInfo("Passwurrd")
  var pws = getPasswords()
  let output = outputData(info,pws)
  if output in passwords:
    passwordToClipboard(output)
