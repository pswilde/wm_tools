#curl https://www.tidetimes.org.uk/exmouth-dock-tide-times-20190101 | grep -E -o ">((High|Low)|([0-9]+:[0-9]+)|([0-9]+\.[0-9]+m))"

import re
import httpclient
import times
import osproc
import sequtils

import ../common
import ../parser
import ../output
import ../model/tides

const icon: string = "🌊 "

proc sortTides(tides: seq[Tide], is_tomorrow: bool = false): seq[Tide] =
  let timenow = now()
  var reltides: seq[Tide]
  for tide in tides:
    if timenow.format("HH:MM") <= tide.time and not is_tomorrow:
      reltides.add(tide)
    elif is_tomorrow:
      reltides.add(tide)
  return reltides


proc getTideData(mytides: TideList, get_tomorrow: bool = false): seq[Tide] =
  var tides: seq[Tide]
  let fnd = re">((High|Low)|([0-9]+:[0-9]+)|([0-9]+\.[0-9]+m))"
  var client = newHttpClient(timeout = 10000)
  var link = replace(mytides.url,re"\%LOC",mytides.location)
  if get_tomorrow:
    let tomdate = now() + initTimeInterval(days = 1)
    link &= "-" & tomdate.format("yyyyMMdd")
  try:
    # Remember to compile with -d:ssl else this won't work
    let resp = client.request(link)
    if resp.status != $Http200 or resp.body == "":
      var data = newInfo("Tideurrl")
      data.full_text =  "Error Response: " & resp.status & ":\nBody:" & resp.body
      discard outputData(data)
      return @[]
    let data = resp.body
    let times = findAll(data,fnd)
    var tide: Tide
    var column = 0
    for idx,time in times:
      if idx == 12:
        break
      let l = len(time) - 1
      if time == ">High" or time == ">Low":
        tide = Tide()
        tide.state = time[1..l]
        column = 1
        continue
      elif column == 1:
        tide.time = time[1..l]
        column = 2
        continue
      elif column == 2:
        tide.height = time[1..l]
        tides.add(tide)
        column = 0
        continue
  except:
    var data = newInfo("Tideurrl")
    data.full_text =  "Unable to get Tide Data : " & getCurrentExceptionMsg()
    discard outputData(data)
  return tides

proc getDesign(tl: TideList): Info =
  var my_tides: seq[string] = @[]
  my_tides.add(tl.location)
  let tides = tl.tides
  for tide in tides:
    let str = icon & tide.state[0] & " " & tide.time & " " & tide.height
    my_tides.add(str)
  var data = newInfo("Tideurrl")
  data.args = my_tides
  return data

proc getTides*(mytides: var TideList, get_tomorrow: bool = false) =
  mytides.tides = mytides.getTideData(get_tomorrow)
  mytides.tides = sortTides(mytides.tides, get_tomorrow)
  if len(mytides.tides) == 0:
    return
  let data = getDesign(mytides)
  var opt_tomorrow = "tomorrow"
  if get_tomorrow:
    opt_tomorrow = "back"
  let args = concat(data.args,@["---",opt_tomorrow])
  let output = outputData(data,args)
  if output == "tomorrow":
    getTides(mytides,true)
  elif output == "back":
    getTides(mytides)
  elif output == "---" or output == "":
    return
  elif output in args:
    let url = replace(mytides.url,re"\%LOC",mytides.location)
    discard execCmd("xdg-open " & url)
  else:
    mytides.location = output
    getTides(mytides)


proc go*() =
  var mytides = parseTideurrlArgs()
  getTides(mytides)

