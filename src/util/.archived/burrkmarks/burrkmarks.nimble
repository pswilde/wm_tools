# Package

version       = "0.1.0"
author        = "Paul Wilde"
description   = "A dmenu/rofi bookmark manager"
license       = "GPL-2.0-or-later"
srcDir        = "src"
bin           = @["burrkmarks"]


# Dependencies

requires "nim >= 1.6.6"
requires "parsetoml >= 0.6.0"
