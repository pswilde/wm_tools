import times

import ../model/info
import ../output

proc getHour(hr: int): string =
  case hr:
    of 1, 13:
      return "one"
    of 2, 14:
      return "two"
    of 3, 15:
      return "three"
    of 4, 16:
      return "four"
    of 5, 17:
      return "five"
    of 6, 18:
      return "six"
    of 7, 19:
      return "seven"
    of 8, 20:
      return "eight"
    of 9, 21:
      return "nine"
    of 10, 22:
      return "ten"
    of 11, 23:
      return "eleven"
    of 0, 12, 24:
      return "twelve"
    else:
      return "error"

proc getMinute(min: int): string =
  case min:
    of 58,59,0,1,2:
      return "oclock"
    of 3,4,5,6,7,53,54,55,56,57:
      return "five"
    of 8,9,10,11,12,48,49,50,51,52:
      return "ten"
    of 13,14,15,16,17,43,44,45,46,47:
      return "quarter"
    of 18,19,20,21,22,38,39,40,41,42:
      return "twenty"
    of 23,24,25,26,27,33,34,35,36,37:
      return "twenty-five"
    of 28,29,30,31,32:
      return "half"
    else:
      return "error"

proc getFuzzyTime(): string =
  let tm = now()
  var hr = tm.hour()
  let min = tm.minute()
  var link = "past"
  if min > 32 :
    link = "to"
    case hr:
      of 23:
        hr = 0
      else:
        hr = hr + 1
  if min >= 58 or min <= 02:
    return getHour(hr) & " " & getMinute(min)
  else:
    return getMinute(min) & " " & link & " " & getHour(hr)

proc getObject(time: string): Info =
  var data = newInfo("Furry Time")
  data.full_text = time
  return data

proc show(time: string, next_fuzzy: bool = false) =
  let data = getObject(time)
  let x = outputData(data)
  if x == time:
    case next_fuzzy:
      of true:
        let t = getFuzzyTime()
        show(t, false)
      else:
        let t = now().format("HH:mm:ss")
        show(t, true)

proc go*() =
  let time = getFuzzyTime()
  show(time)

