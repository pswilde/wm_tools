import times
import osproc
import re

import ../common
import ../output

const default_format = "yyyy-MM-dd"
const cal_pos_x = "20"
const cal_pos_y = "20"

proc getObject(date: string): Info =
  var data = newInfo("Calendurr")
  data.full_text = date
  return data

proc newCalendar(): string =
  var c = """yad --calendar \
             --undecorated --fixed --close-on-unfocus --no-buttons \
             --width="222" --height="193" \
             --posx="%pos_x" --posy="%pos_y" \
             --title="yad-calendar" --borders 0 > /dev/null
          """
  return c

proc openCalendar() =
  var c = newCalendar()
  c = replace(c,re"%pos_x", cal_pos_x)
  c = replace(c,re"%pos_y", cal_pos_y)
  discard execCmd(c)

proc getDate*() =
  let date_today = times.now().format(default_format)
  let data = getObject(date_today)
  let output = outputData(data)
  if output == date_today:
    openCalendar()

proc go*() =
  getDate()
  

