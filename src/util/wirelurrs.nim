import os
import osproc
import strutils
import sequtils

import ../common
import ../output

const get_ssid_cmd = "iwgetid -r"
const mng_cmd = "alacritty -e nmtui-connect"

proc getSsid(): string =
  let ssid = execCmdEx(get_ssid_cmd)
  return strip(ssid.output)

proc getSignalQuality(): string =
  let wl = readFile("/proc/net/wireless")
  let ln = splitLines(wl)[2]
  let links = split(ln,"  ")
  var qual = strip(links[1])
  qual = replace(qual,".","")
  return "[" & qual & "]"

proc getWifi(nic: string): (string, string) =
  let ssid = getSsid()
  if ssid == "":
    return ("disconnected", "disconnected")
  let quality = getSignalQuality()
  return (ssid, quality)

proc getObject(): Info =
  var data = newInfo("Wirelurrs")
  return data

proc getWifiInfo*(nics: seq[string])  =
  var lst: seq[string] = @[]
  for nic in nics:
    let (essid, quality) = getWifi(nic)
    lst.add(nic & ":" & quality & " " & essid)
  let data = getObject()
  let args = concat(lst,@["---", "manage","exit"])
  let output = outputData(data, args)
  if output in lst:
    discard execCmd(mng_cmd)
  case output:
    of "manage":
      discard execCmd(mng_cmd)
    of "exit":
      return
    of "---":
      return

proc getWiFiNICs(): seq[string] =
  var my_nics: seq[string] = @[]
  for path in walkDir("/sys/class/net/"):
    if dirExists(path.path & "/wireless"):
      let nic = path.path.replace("/sys/class/net/","")
      my_nics.add(nic)
  return my_nics

proc go*() =
  let my_nics = getWiFiNICs()
  if len(my_nics) > 0:
    getWifiInfo(my_nics)
  else:
    var data = getObject()
    data.full_text = "No WLAN"
    discard outputData(data)

