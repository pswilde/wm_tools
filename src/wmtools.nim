# This is just an example to get you started. A typical binary package
# uses this file as the main entry point of the application.
#

import common
import parser
import dispatcher

when isMainModule:
  parseArgs()
  dispatch myConfig
